from datetime import datetime, timezone
import ccxt
from ccxt.base.errors import InvalidOrder
from exchanges import config
from enum import Enum
import time
from exchanges.base import BaseExchangeFlaskView
import logging
import os

logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))
log = logging.getLogger(__name__)

class Order(Enum):
    OPEN_LONG = 'buy'
    OPEN_SHORT = 'sell'
    CLOSE_LONG = 'sell'
    CLOSE_SHORT = 'buy'


class Bybit():
    exchange = None

    def __init__(self):
        exchange_id = 'bybit'
        exchange_class = getattr(ccxt, exchange_id)
        self.exchange = exchange_class({
            'apiKey': config.BYBIT_API_KEY,
            'secret': config.BYBIT_SECRET,
        })

    def get_exchange(self):
        return self.exchange

    def _fetch_position(self, symbol):
        position = self.get_exchange().fetchPositions(symbols=[symbol])[0]
        return position if position['size'] else None

    def _fetch_ticker(self, symbol):
        ticker = self.get_exchange().fetchTicker(symbol)

        return ticker

    def _retry_limit_order_until_fill(self, symbol: str, order_type: Order, contracts: float, params):
        start = time.time()
        position = self._fetch_position(symbol)

        if not position:
            desired_position_size = contracts
        else:
            position_size = float(position['size'])
            if order_type == 'buy':
                desired_position_size = position_size + contracts
            if order_type == 'sell':
                desired_position_size = 0 if position_size - \
                    contracts < 0 else position_size - contracts

        desired_position_size = desired_position_size
        pending_fills = contracts
        prev_order = None
        while pending_fills:
            ticker = self._fetch_ticker(symbol)
            if prev_order:
                prev_order = self.get_exchange().fetchOrder(
                    prev_order['id'], symbol=symbol)

                if prev_order['status'] == 'closed':
                    break

                try:
                    self.get_exchange().cancelOrder(
                        prev_order['id'], symbol=symbol)
                    end = time.time()
                    # stop exeuction if 60s elapsed
                    if end - start > 60:
                        log.info(f'Order window timed out') 
                        break
                    prev_order = self.get_exchange().fetchOrder(
                        prev_order['id'], symbol=symbol)
                    pending_fills -= prev_order['filled']
                except InvalidOrder:
                    # order filled
                    pass
            bbo = ticker['bid'], ticker['ask']
            limit_price = bbo[0] if order_type == 'buy' else bbo[1]
            try:
                log.info('placing order')
                prev_order = self.get_exchange().createLimitOrder(
                    symbol, order_type, pending_fills, limit_price, params)
                time.sleep(3)

            except InvalidOrder as e:
                if "Size too small" in str(e):
                    break
                else:
                    log.info(f'invalid order: {e}')
                pass


        return "Orders Placed"

    def split_limit_orders(self, symbol: str, order_type: Order, amount_usd: float, low: float, high: float, split=5):
        ticker = self._fetch_ticker(symbol)
        params = {
            "post_only": True
        }
        if order_type in [Order.CLOSE_LONG, Order.CLOSE_SHORT]:
            params['reduce_only'] = True

        # validations
        assert high > low, "High is lower than low"

        single_split_amount = (high - low) / split
        size_per_split = amount_usd / split / ticker['close']
        limit_price = low
        while limit_price >= low and limit_price <= high:
            self.get_exchange().createLimitOrder(symbol, order_type,
                                                 size_per_split, limit_price, params)
            limit_price += single_split_amount
        return "Orders placed"

    def split_close_limit_orders(self, symbol: str, close_percentage: float, low: float, high: float, split=5):
        ticker = self._fetch_ticker(symbol)
        params = {
            "post_only": True,
            "reduce_only": True
        }

        # validations
        assert high > low, "High is lower than low"
        assert close_percentage >= 0.01 and close_percentage <= 1, "Close percentage must be within 0.01-1"

        position = self._fetch_position(symbol)
        position_size = float(position['size'])
        order_type = 'sell' if position['side'].lower() == 'buy' else 'buy'

        single_split_amount = (high - low) / split
        size_per_split = (position_size / split) * close_percentage

        limit_price = low
        while limit_price >= low and limit_price <= high:
            self.get_exchange().createLimitOrder(symbol, order_type,
                                                 size_per_split, limit_price, params)
            limit_price += single_split_amount

    def open_at_market_post(self, symbol: str, order_type: Order, amount_usd: float):
        ticker = self._fetch_ticker(symbol)
        params = {
            "post_only": True
        }

        open_size = amount_usd / ticker['close']

        return self._retry_limit_order_until_fill(symbol, order_type, open_size, params)

    def close_at_market_post(self, symbol: str, close_percentage: float):
        position = self._fetch_position(symbol)
        params = {
            "post_only": True,
            "reduce_only": True
        }
        if not position:
            log.info(f'No open position for {symbol}')
            return

        # assertions
        assert close_percentage >= 0.01 and close_percentage <= 1, "Close percentage must be within 0.01-1"

        position_size = float(position['size'])
        close_size = position_size * close_percentage

        order_type = 'sell' if position['side'].lower() == 'buy' else 'buy'

        return self._retry_limit_order_until_fill(symbol, order_type, close_size, params)


class BybitView(BaseExchangeFlaskView, Bybit):
    exchange_id = 'bybit'

    def before_open_at_market_post_route(self):
        if self.data.get('strategy'):
            strategy = self.data.pop('strategy') 
            if strategy == 'daily-vwap':
                utchour = datetime.now(timezone.utc).hour
                if utchour > 10:
                    return self.open_at_market_post(**self.data)
                else:
                    return "not in trading timezone"
        else:
            self.open_at_market_post(**self.data)

    def before_close_at_market_post_route(self):
        if self.data.get('strategy'):
            strategy = self.data.pop('strategy') 
            if strategy == 'daily-vwap':
                utchour = datetime.now(timezone.utc).hour
                if utchour > 10:
                    return self.close_at_market_post(**self.data)
                else:
                    return "not in trading timezone"
        else:
            self.close_at_market_post(**self.data)
