# Flask server for executing trades

Exposes a webhook url

https://dvv1rb6o40.execute-api.ap-northeast-2.amazonaws.com/dev/webhook

## Installation
Create a config.py file with these variables

* WEBHOOK_PASSPHRASE
* API_KEY (binance)
* API_SECRET (binance)

## Deployment
Make sure virtualenv is activated  and dependencies are installed before deployment

* zappa deploy dev
* zappa update dev