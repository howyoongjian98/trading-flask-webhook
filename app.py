
from flask import Flask

from exchanges.ftx import FTXView
from exchanges.bybit import BybitView

app = Flask(__name__)

FTXView.register(app, route_base='ftx')
BybitView.register(app, route_base='bybit')

if __name__ == '__main__':
    app.run(host='0.0.0.0')


@app.route('/')
def home():
    return {
        "status": 200
    }
